SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `accounts` ADD `timevote` BIGINT(100);
ALTER TABLE `accounts` ADD `votes` INT(4);
ALTER TABLE `accounts` ADD `points` INT(4);
ALTER TABLE `accounts` ADD `messages` TEXT(0);

-- ----------------------------
-- Table structure for `panty_comments`
-- ----------------------------
DROP TABLE IF EXISTS `panty_comments`;
CREATE TABLE `panty_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL DEFAULT '0',
  `author` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`,`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of panty_comments
-- ----------------------------
INSERT INTO `panty_comments` VALUES ('1', '6', 'Ramlethal', 'Les commentaires fonctionnent également. Un administrateur peut les supprimer.');

-- ----------------------------
-- Table structure for `panty_news`
-- ----------------------------
DROP TABLE IF EXISTS `panty_news`;
CREATE TABLE `panty_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of panty_news
-- ----------------------------
INSERT INTO `panty_news` VALUES ('1', 'Bienvenue sur Panty !', 'Si vous voyez cette page, c\'est que vous avez correctement installé le CMS Panty.<br />\r\nJe vous souhaite ainsi une bonne utilisation du CMS.<br />\r\n<br />\r\nEn cas de doute concernant son fonctionnement, reportez-vous au topic sur le forum E-Dofus.com.<br />\r\nPour toute question, créez un topic sur ce dernier, je vous répondrais au plus vite.<br />\r\n<br />\r\nRamlethal', 'Ramlethal', '3');
