<?php
/***
 * Panty - Change the design like you change your panty !
 * Copyright Ramlethal 2013-2014
 * Fichier de configuration
 ***/
 
/* 
En cas de problème de configuration, vous pouvez poster un topic d'aide sur le forum E-Dofus.fr.
Attention à bien rentrer les IDD et les IDP starpass !
N'hésitez pas à consulter le topic principal sur E-Dofus pour vous tenir au courrant des mises à jour !
Bonne utilisation, 
Ramlethal.
*/

/* Le titre est affiché sur toute les pages, dand l'entête du Navigateur etc. N'hésitez pas 
à rajouter des mots clés pour améliorer le référencement de votre serveur sur Google. */
define('TITLE', 'Panty'); // Titre du site/Nom du serveur

/* Toute vos informations concernant votre base de données restent évidemment confidentielles.
Les informations sont les mêmes que pour votre émulateur, étant donné que le CMS utilise
les données de l'émulateur (logique, non ?) */
define('DB_HOST', '127.0.0.1'); // Adresse de la base de données
define('DB_NAME', 'pants'); // Nom de la base de données
define('DB_USER', 'root'); // User de la base de données
define('DB_PASSWORD', ''); // Mot de passe de la base de données

/* L'URL du CMS doit être mis correctement. Si vous constatez un bug graphique (tout est blanc, pas d'images...),
c'est que le problème vient probablement de là. C'est grâce à cette URL que les images sont chargées etc. */
if (!defined('TMVC_URL'))
	define('TMVC_URL', 'http://localhost/Panty/'); // URL du CMS, avec le "/" à la fin

/* Ici vous pourrez paramétrer tout ce qui touche au gain de points, les commentaires vous orienteront.
Il n'y a pas grand chose à ajouter, tout est commenté.
Pour Starpass et sa configuration, basez vous sur ça : http://puu.sh/43Ggw */
define('SHOP_TOKENS', 10); // Nombre de points gagnés pour un code audio
define('VOTE_TOKENS', 2); // Nombre de points gagnés pour un vote

define('VOTE_URL', 'http://localhost/Panty/index.php'); // URL de vote
define('BOARD_URL', 'http://localhost/Panty/index.php'); // URL de votre forum
define('DOFUS_URL', 'http://localhost/Panty/index.php'); // URL de téléchargement pour l'installateur DOFUS 1.29.1
define('LAUNCHER_URL', 'http://localhost/Panty/index.php'); // URL de téléchargement pour le launcher/l'installateur de votre serveur

define('IDP', 17013); // IDP starpass
define('IDD', 153985); // IDD starpass

/* Variable vous permettant de changer le thème du CMS. Le nom du thème est égal au nom du dossier contenu dans system > smarty > templates.
Faites attention aux majuscules/minuscules. Si vous avez une erreur concernant le thème, c'est que le nom entré n'est pas le bon. */
if (!defined('PANTY_THEME'))
	define('PANTY_THEME', 'ragefull'); // Thème du CMS

/* Pas besoin d'en rajouter. Si vous avez une boutique en jeu, changez TRUE pour FALSE.
Cela permet de désactiver la boutique via le CMS, qui peut poser problème lorsque les émulateurs
utilisent une boutique en jeu (et ne chargent donc pas les live_action). */	
define('USE_SHOP', TRUE); // Utilisation de la boutique ou pas ?