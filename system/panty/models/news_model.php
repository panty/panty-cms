<?php
/**
 * news_model.php
 *
 * Page intégrant les requêtes niveau news
 *
 * @package		Panty
 * @author		Ramlethal
 */
class News_Model extends TinyMVC_Model
{
	function get_news()
	{
		$this->db->query('select * from panty_news ORDER BY id DESC LIMIT 0,5');
		$array_news = array();
		$i          = 1;
		while ($row = $this->db->next()) {
			$array_news[$i]['id']      = $row['id'];
			$array_news[$i]['title']   = htmlentities($row['title'], ENT_QUOTES, "UTF-8");
			$array_news[$i]['content'] = $row['content'];
			$array_news[$i]['author']  = htmlentities($row['author'], ENT_QUOTES, "UTF-8");
			$array_news[$i]['type']    = $row['type'];
			$i++;
		} //$row = $this->db->next()
		return $array_news;
	}
	function view($id)
	{
		if ($this->db->query('select COUNT(*) from panty_news where id=?', array(
			$id
		)) == 1):
			return $this->db->query_one('select * from panty_news where id=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function get_comments($id)
	{
		if ($this->db->query('select COUNT(*) from panty_news where id=?', array(
			$id
		)) == 1):
			$this->db->query('select * from panty_comments where news_id=? ORDER BY id DESC ', array(
				$id
			));
			$array_comments = array();
			$i              = 1;
			while ($row = $this->db->next()) {
				$array_comments[$i]['id']      = $row['id'];
				$array_comments[$i]['author']  = htmlentities($row['author'], ENT_QUOTES, "UTF-8");
				$array_comments[$i]['content'] = htmlentities($row['content'], ENT_QUOTES, "UTF-8");
				$i++;
			} //$row = $this->db->next()
			return $array_comments;
		else:
			return FALSE;
		endif;
	}
	function add_comment($id, $message, $user)
	{
		return $this->db->insert('panty_comments', array(
			'news_id' => $id,
			'author' => $user,
			'content' => $message
		));
	}
	function delete_comment($id)
	{
		if ($this->db->query('select COUNT(*) from panty_comments where id=?', array(
			$id
		)) == 1):
			return $this->db->query('delete from panty_comments where id=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function delete_new($id)
	{
		if ($this->db->query('select COUNT(*) from panty_news where id=?', array(
			$id
		)) == 1):
			return $this->db->query('delete from panty_news where id=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function get_all_news()
	{
		$this->db->query('select * from panty_news ORDER BY id DESC');
		$array_news = array();
		$i          = 1;
		while ($row = $this->db->next()) {
			$array_news[$i]['id']     = $row['id'];
			$array_news[$i]['title']  = htmlentities($row['title'], ENT_QUOTES, "UTF-8");
			$array_news[$i]['author'] = htmlentities($row['author'], ENT_QUOTES, "UTF-8");
			$array_news[$i]['type']   = $row['type'];
			$i++;
		} //$row = $this->db->next()
		return $array_news;
	}
	function add_news($message, $title, $type, $user)
	{
		return $this->db->insert('panty_news', array(
			'author' => $user,
			'title' => $title,
			'type' => $type,
			'content' => $message
		));
	}
}