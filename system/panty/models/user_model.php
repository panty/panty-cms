<?php
/**
 * user_model.php
 *
 * Page intégrant les requêtes niveau utilisateurs
 *
 * @package		Panty
 * @author		Ramlethal
 */
class User_Model extends TinyMVC_Model
{
	function get_user($username, $password)
	{
		if ($this->db->query('select COUNT(*) from accounts where account=? AND pass=?', array(
			$username,
			$password
		)) == 1):
			return $this->db->query_one('select * from accounts where account=? AND pass=?', array(
				$username,
				$password
			));
		else:
			return FALSE;
		endif;
	}
	function get_account_characters($account_id)
	{
		$this->db->select('*');
		$this->db->from('personnages');
		$this->db->where('account', $account_id);
		$this->db->orderby('xp');
		$this->db->query();
		$array_perso = array();
		$alignement  = array(
			0 => '0',
			1 => '1',
			2 => '2',
			3 => '3'
		);
		$sexe        = array(
			0 => 'm',
			1 => 'f'
		);
		$classe      = array(
			1 => 'feca',
			2 => 'osa',
			3 => 'enu',
			4 => 'sram',
			5 => 'xel',
			6 => 'eca',
			7 => 'eni',
			8 => 'iop',
			9 => 'cra',
			10 => 'sadi',
			11 => 'sacri',
			12 => 'pand'
		);
		$i           = 1;
		while ($row = $this->db->next()) {
			$array_perso[$i]['pos']        = $i;
			$array_perso[$i]['id']         = $row['guid'];
			$array_perso[$i]['name']       = $row['name'];
			$array_perso[$i]['level']      = $row['level'];
			$array_perso[$i]['sexe']       = $sexe[$row['sexe']];
			$array_perso[$i]['class']      = $classe[$row['class']];
			$array_perso[$i]['account']    = $row['account'];
			$array_perso[$i]['alignement'] = $alignement[$row['alignement']];
			$array_perso[$i]['honor']      = $row['honor'];
			$array_perso[$i]['xp']         = $row['xp'];
			$i++;
		} //$row = $this->db->next()
		return $array_perso;
	}
	function user_data($id)
	{
		if ($this->db->query('select COUNT(*) from accounts where guid=?', array(
			$id
		)) == 1):
			return $this->db->query_one('select * from accounts where guid=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function char_data($id)
	{
		if ($this->db->query('select COUNT(*) from personnages where guid=?', array(
			$id
		)) == 1):
			return $this->db->query_one('select * from personnages where guid=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function guild_char_data($id)
	{
		if ($this->db->query('select COUNT(*) from guild_members where guid=?', array(
			$id
		)) == 1):
			return $this->db->query_one('select * from guild_members where guid=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function guild_data($id)
	{
		if ($this->db->query('select COUNT(*) from guilds where id=?', array(
			$id
		)) == 1):
			return $this->db->query_one('select * from guilds where id=?', array(
				$id
			));
		else:
			return FALSE;
		endif;
	}
	function change_pass($id, $new_pass)
	{
		$this->db->where('guid', $id);
		return $this->db->update('accounts', array(
			'pass' => $new_pass
		));
	}
	function change_message($id, $message)
	{
		$this->db->where('guid', $id);
		return $this->db->update('accounts', array(
			'message' => $message
		));
	}
	function add_vote($id, $date_now)
	{
		$datas  = $this->db->query_one('select * from accounts where guid=?', array(
			$id
		));
		$tokens = $datas['points'] + VOTE_TOKENS;
		$votes  = $datas['votes'] + 1;
		$this->db->where('guid', $id);
		$this->db->update('accounts', array(
			'votes' => $votes,
			'points' => $tokens,
			'timevote' => $date_now
		));
		return $tokens;
	}
	function already_used($username, $pseudo, $email)
	{
		$request     = $this->db->query('select * from accounts where account=? OR pseudo=? OR email=?', array(
			$username,
			$pseudo,
			$email
		));
		$request_num = $this->db->num_rows();
		if ($request_num == 1):
			return TRUE;
		else:
			return FALSE;
		endif;
	}
	function create_user($username, $password, $pseudo, $email, $quest, $answ)
	{
		return $this->db->insert('accounts', array(
			'account' => $username,
			'pass' => $password,
			'pseudo' => $pseudo,
			'email' => $email,
			'question' => $quest,
			'reponse' => $answ
		));
	}
	function add_tokens($id)
	{
		$datas  = $this->db->query_one('select * from accounts where guid=?', array(
			$id
		));
		$tokens = $datas['points'] + SHOP_TOKENS;
		$this->db->where('guid', $id);
		$this->db->update('accounts', array(
			'points' => $tokens
		));
		return $tokens;
	}
}