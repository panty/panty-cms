<?php
/**
 * administration.php
 *
 * Page permettant à l'utilisateur de gérer les données en BDD
 * Ex : news, boutique
 *
 * @package       Panty
 * @author        Ramlethal
 */
class Administration_Controller extends TinyMVC_Controller
{
	function index()
	{
		if (!isset($_SESSION['logged']) || $_SESSION['gmlvl'] < 1) {
			header("Location: " . TMVC_URL . "index.php/news/index");
			exit;
		} //!isset($_SESSION['logged']) || $_SESSION['gmlvl'] < 1
		$this->smarty->display(PANTY_THEME . '/news.tpl');
	}
	function news()
	{
		if (!isset($_SESSION['logged']) || $_SESSION['gmlvl'] < 1) {
			header("Location: " . TMVC_URL . "index.php/news/index");
			exit;
		} //!isset($_SESSION['logged']) || $_SESSION['gmlvl'] < 1
		$this->load->model('News_Model', 'news');
		if (isset($_POST['news']) && strlen($_POST['message']) >= 20 && strlen($_POST['title']) >= 2) {
			$message = nl2br(trim($_POST['message']));
			$title   = $_POST['title'];
			$type    = (int) $_POST['type'];
			$user    = htmlentities($_SESSION['pseudo']);
			$this->news->add_news($message, $title, $type, $user);
		} //isset($_POST['news']) && strlen($_POST['message']) >= 20 && strlen($_POST['title']) >= 2
		$news = $this->news->get_all_news();
		$this->smarty->assign('array_news', $news);
		$this->smarty->display(PANTY_THEME . '/manage-news.tpl');
	}
	function delete_new()
	{
		if (!isset($_SESSION['logged']) || $_SESSION['gmlvl'] < 1) {
			header("Location: " . TMVC_URL . "index.php/news/index");
			exit;
		} //!isset($_SESSION['logged']) || $_SESSION['gmlvl'] < 1
		$this->load->library('uri');
		$this->load->model('News_Model', 'news');
		$id     = (int) $this->uri->segment(4);
		$delete = $this->news->delete_new($id);
		if ($delete):
			$dat = array(
				"title" => "Suppr&eacute;ssion r&eacute;ussie",
				"content" => "L'article a &eacute;t&eacute; supprim&eacute; avec succ&egrave;s !"
			);
		else:
			$dat = array(
				"title" => "Erreur",
				"content" => "L'article n'existe pas ou plus."
			);
		endif;
		$this->smarty->assign($dat);
		$this->smarty->display(PANTY_THEME . '/message.tpl');
	}
	function shop()
	{
		$dat = array(
			"title" => "Suppr&eacute;ssion r&eacute;ussie",
			"content" => "Le commentaire a &eacute;t&eacute; supprim&eacute; avec succ&egrave;s !"
		);
		$this->smarty->assign($dat);
		$this->smarty->display(PANTY_THEME . '/message.tpl');
	}
}