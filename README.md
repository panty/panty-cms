# Panty #

## Qu'est-ce que c'est ? ##
**Panty** est un CMS DOFUS 1.29.1 adapté à **Ancestra Remake** et ses dérivés commencé il y a maintenant plusieurs mois. Propulsé par TinyMVC, framework possédant une architecture MVC (comme son nom l'indique), et couplé à Smarty, moteur de template, Panty propose une gestion multi-thème complète. 
En effet, avec un minimum de connaissances en HTML et un minimum de tact, vous pourrez très vite adapter vos propres designs rapidement à Panty sans avoir besoin de vous occuper des actions PHP.

## Installation ##
1. Commencez par télécharger le CMS. Pour cela, rendez-vous dans la section "Downloads" tout en bas du menu gauche. Ici, vous pourrez télécharger la dernière version du CMS en cliquant sur "Download repository".
2. Dézippez le contenu de l'archive fraîchement téléchargée dans votre répertoire web (où vous souhaitez utiliser Panty).
3. Rendez-vous dans le dossier system < panty < configs et éditez le fichier Panty.php. Suivez les instruction dans les commentaires du fichiers. Ils vous guideront dans cette tâche.
4. Patchez votre base de données avec le fichier SQL présent dans votre dossier Panty (à la racine).
Si il n'y est pas (ce serait étrange...), voici le code contenu dans le fichier :

```
#!sql


SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `accounts` ADD `timevote` BIGINT(100);
ALTER TABLE `accounts` ADD `votes` INT(4);
ALTER TABLE `accounts` ADD `points` INT(4);
ALTER TABLE `accounts` ADD `messages` TEXT(0);

-- ----------------------------
-- Table structure for `panty_comments`
-- ----------------------------
DROP TABLE IF EXISTS `panty_comments`;
CREATE TABLE `panty_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL DEFAULT '0',
  `author` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`,`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of panty_comments
-- ----------------------------
INSERT INTO `panty_comments` VALUES ('1', '6', 'Ramlethal', 'Les commentaires fonctionnent également. Un administrateur peut les supprimer.');

-- ----------------------------
-- Table structure for `panty_news`
-- ----------------------------
DROP TABLE IF EXISTS `panty_news`;
CREATE TABLE `panty_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of panty_news
-- ----------------------------
INSERT INTO `panty_news` VALUES ('1', 'Bienvenue sur Panty !', 'Si vous voyez cette page, c\'est que vous avez correctement installé le CMS Panty.<br />\r\nJe vous souhaite ainsi une bonne utilisation du CMS.<br />\r\n<br />\r\nEn cas de doute concernant son fonctionnement, reportez-vous au topic sur le forum E-Dofus.com.<br />\r\nPour toute question, créez un topic sur ce dernier, je vous répondrais au plus vite.<br />\r\n<br />\r\nRamlethal', 'Ramlethal', '3');


```

## Système de template ##
Pour adapter un template à Panty, il vous faut : des connaissances en HTML, savoir lire une documentation & avoir du temps (entre 30 minutes et 2 heures en fonctions des connaissances et de la vitesse de compréhension)

Les templates se situent dans le dossier system < smarty < templates.
Les assets (images, css, js) se situent dans le dossier assets.

Dans ces deux dossiers, on trouve un ou plusieurs dossiers. Ces derniers portent le nom du template (sauf pour global dans assets qui correspond aux images globales du CMS).
Ainsi, pour créer un template, vous devrez créer des dossiers portant le nom du template (afin de pouvoir l'utiliser dans la configuration du CMS).

Les fichiers template contenus dans le dossier Smarty sont au format *.tpl, mais on peut les ouvrir avec n'importe quel éditeur de texte.
Deux types de fichiers sont observables : le layout et le fichier d'extension. Le fichier layout, appelé layout.tpl, correspond au corps du design, c'est le design sans le contenu dit "dynamique" (qui change d'une page à l'autre). C'est la base du design, en gros.
Dans ce dernier, on trouve des lignes d'extension, type :

```
#!html

{block name=body}{/block}
```

Cette ligne signifie qu'elle sera étendue par les autres fichiers *.tpl, et plus exactement par le contenu contenu entre les lignes 


```
#!html

{block name=body}

{/block}
```

Vous observerez que les fichiers *.tpl d'extension commencent par la ligne :

```
#!html

{extends file="$THEME/layout.tpl"}
```

Tous vos fichiers visant à étendre le layout doivent commencer par cette ligne. Elle permet d'inclure les "block" au layout, et donc d'inclure le contenu au layout. 
Sur Panty, il y a deux blocks :

```
#!html

{block name=title}Titre de la page{/block}
```
Qui étend le layout en définissant le nom de la page, qui sera utilisé dans l'entête du navigateur et dans le corps de la page.


```
#!html

{block name=body}
Contenu de la page...
{/block}
```
Qui étend le corps de page en y insérant du contenu (news, règlement, texte...).
Pour toute les fonctions concernant l'affichage des pages, vous pouvez soit regarder dans le template de base ("ragefull"), soit vous rendre sur le site de Smarty pour lire la documentation : http://www.smarty.net/docsv2/fr/


## En cas de problème... ##
Première solution, vous vous rendez dans l'onglet "Issues" du menu de gauche (ou cliquer sur ce lien : https://bitbucket.org/panty/panty/issues/new). Il vous permettra de signaler un bug, et permettra à tous les autre futurs utilisateurs de voir votre issue. Ainsi, ils pourront corriger l'erreur que vous avez eu précédemment sans avoir à poster un message. C'est un peu une base de données d'erreurs dynamique !

Deuxième solution, vous postez sur le forum E-Dofus.com, cependant je vous demanderai de préférer la première solution à celle-ci car elle me permet de tout concentrer à un endroit et de voir l'évolution du CMS (concernant les bugs et les retours).